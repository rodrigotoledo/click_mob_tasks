# frozen_string_literal: true

class TaskLogsController < ApplicationController
  before_action :authenticate_user!
  # GET /task_logs.json
  def index
    respond_to do |format|
      format.json { render json: TaskLogDatatable.new(view_context) }
    end
  end

  # POST /task_logs
  # POST /task_logs.json
  def create
    @task_log = TaskLog.new(task_log_params)
    @task_log.user_id = current_user.id

    respond_to do |format|
      if @task_log.save
        format.html { redirect_to edit_task_path(@task_log.task_id), notice: I18n.t(:success_operation) }
        format.json { render :show, status: :created, location: @task_log }
      else
        format.html { render edit_task_path(@task_log.task_id) }
        format.json { render json: @task_log.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_task_log
    @task_log = TaskLog.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def task_log_params
    params.require(:task_log).permit(:task_id, :description, :finished)
  end
end
