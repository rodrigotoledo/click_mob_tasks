# frozen_string_literal: true

module UsersHelper
  def users_options_by_role
    users = User.order('name DESC')
    users = users.where(id: current_user.id) unless current_user.administrador?
    users.collect { |u| [u.name, u.id] }
  end
end
