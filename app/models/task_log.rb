# frozen_string_literal: true

class TaskLog < ApplicationRecord
  belongs_to :task
  belongs_to :user

  validates :description, presence: true
  validates_associated :user
  validates_associated :task

  after_create :close_task

  protected

  def close_task
    return true unless finished?
    task.update_attribute(:status, 'finalizado')
  end
end
