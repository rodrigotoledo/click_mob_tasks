# frozen_string_literal: true

json.set! :data do
  json.array! @task_logs do |task_log|
    json.partial! 'task_logs/task_log', task_log: task_log
    json.url  "
              #{link_to 'Show', task_log}
              #{link_to 'Edit', edit_task_log_path(task_log)}
              #{link_to 'Destroy', task_log, method: :delete, data: { confirm: 'Are you sure?' }}
              "
  end
end
