# frozen_string_literal: true

class TaskLogDatatable < AjaxDatatablesRails::Base
  extend Forwardable

  def_delegators :@view, :params, :current_user

  def view_columns
    @view_columns ||= {
      description: { source: 'TaskLog.description' },
      user: { source: 'User.name' }
    }
  end

  def searchable_columns
    @searchable_columns ||= []
  end

  def data
    records.map do |record|
      {
        description: record.description,
        user: record.user.name
      }
    end
  end

  def get_raw_records
    task_logs = TaskLog.where(task_id: params[:task_id]).order('created_at DESC')
    task_logs = task_logs.joins(:task).where('tasks.user_id = ?', current_user.id) unless current_user.administrador?

    if !params[:search].blank? && !params[:search][:value].blank?
      search = params[:search][:value]

      task_logs = task_logs.joins(:user).where('(task_logs.description ILIKE :search OR users.name ILIKE :search)', search: "#{search}%")
    end

    task_logs
  end
end
