# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
do ->
  tasksTable = null

  document.addEventListener 'turbolinks:load', ->
    tasksTable = $('#tasks-datatable').dataTable
      language:
          sEmptyTable: "Nenhum registro encontrado",
          sInfo: "Mostrando de _START_ até _END_ de _TOTAL_ registros",
          sInfoEmpty: "Mostrando 0 até 0 de 0 registros",
          sInfoFiltered: "(Filtrados de _MAX_ registros)",
          sInfoPostFix: "",
          sInfoThousands: ".",
          sLengthMenu: "_MENU_ resultados por página",
          sLoadingRecords: "Carregando...",
          sProcessing: "Processando...",
          sZeroRecords: "Nenhum registro encontrado",
          sSearch: "Pesquisar",
          oPaginate:
              sNext: "Próximo",
              sPrevious: "Anterior",
              sFirst: "Primeiro",
              sLast: "Último"
          oAria:
              sSortAscending: ": Ordenar colunas de forma ascendente",
              sSortDescending: ": Ordenar colunas de forma descendente"
      processing: true
      ordering: false
      serverSide: true
      searching: true
      destroy: true
      ajax: $('#tasks-datatable').data('source')
      pagingType: 'full_numbers'
      columns: [
        {data: 'title'}
        {data: 'user'}
        {data: 'status'}
        {data: 'actions'}
      ]
  document.addEventListener 'turbolinks:before-cache', ->
    if tasksTable.length == 1
      tasksTable.destroy
