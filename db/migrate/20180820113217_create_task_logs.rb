# frozen_string_literal: true

class CreateTaskLogs < ActiveRecord::Migration[5.2]
  def change
    create_table :task_logs do |t|
      t.references :task, foreign_key: true
      t.references :user, foreign_key: true
      t.text :description
      t.boolean :status, default: false

      t.timestamps
    end
  end
end
