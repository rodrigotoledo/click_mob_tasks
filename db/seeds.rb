# frozen_string_literal: true

require 'faker'

30.times do
  User.create(name: Faker::Name.unique.name, email: Faker::Internet.email,
              password: 'asdqwe123', password_confirmation: 'asdqwe123', kind_of: 'operador')
end

User.create(name: 'Rodrigo Toledo', email: 'rodrigo@rtoledo.inf.br',
            password: 'asdqwe123', password_confirmation: 'asdqwe123', kind_of: 'administrador')
User.create(name: 'Eduardo Martins', email: 'eduardo.martins@didactica.com.br',
            password: 'asdqwe123', password_confirmation: 'asdqwe123', kind_of: 'administrador')
User.create(name: 'Operador - Rodrigo Toledo', email: 'agc.rodrigo@gmail.com',
            password: 'asdqwe123', password_confirmation: 'asdqwe123', kind_of: 'operador')

30.times do
  Task.create(
    user_id: User.order('RANDOM()').first.id,
    title: Faker::Lorem.sentence,
    description: Faker::Lorem.paragraph(2),
    scheduled_to: Faker::Time.forward(23, :morning),
    status: 'em_andamento'
  )
end
