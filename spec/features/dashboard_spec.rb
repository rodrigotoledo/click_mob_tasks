# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Visit application and show Dashboard page' do
  before(:all) do
    @admin_user = create(:user)
  end

  describe 'Show dashboard of administrator page' do
    it 'Show dashboard with access message' do
      visit '/'

      fill_in 'user_email', with: @admin_user.email
      fill_in 'user_password', with: @admin_user.password
      click_button 'Acessar'

      expect(page).to have_content('Acesso efetuado com sucesso!')
      expect(page).to have_link('Dashboard de Tarefas')
      expect(page).to have_link('Tarefas')
      expect(page).to have_link('Controle de Acessos')
      expect(page).to have_link('Sair')
      expect(page).not_to have_button('Acessar')
    end
  end
end
