# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    name { Faker::Name.unique.name }
    email { Faker::Internet.email }
    password { 'asdqwe123' }
    password_confirmation { 'asdqwe123' }
    kind_of { 'administrador' }
  end

  factory :operator, class: User do
    name { Faker::Name.unique.name }
    email { Faker::Internet.email }
    password { 'asdqwe123' }
    password_confirmation { 'asdqwe123' }
    kind_of { 'operador' }
  end
end
